// Tạo 1 mảng có 100 phần tử toàn bộ là số 0, chuyển mảng vừa tạo thành mảng mới có giá trị từ 0->99
// , lọc ra những số chia hết cho 5 rồi tính tổng những số còn lại.

const numberList = new Array(100);
const setNumber = numberList.fill(0);

const valueConversion = Array.from(setNumber.keys());

let sum = 0;
valueConversion.forEach((number) => {
  if (number % 5 === 0) {
    console.log(number);
  } else {
    sum += number;
  }
});

console.log(sum);
