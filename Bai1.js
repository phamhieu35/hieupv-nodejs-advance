// Bài 1: Sắp xếp mảng sau theo thứ tự độ tuổi giảm dần
const ageList = [
  { name: "name1", age: 12 },
  { name: "name2", age: 20 },
  { name: "name3", age: 15 },
  { name: "name4", age: 10 },
  { name: "name4", age: 27 },
];

console.log(ageList.sort((a, b) => b.age - a.age));
