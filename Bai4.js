// Viết code nhóm các user có cùng name và cộng tổng số count thành một mảng mới

const arrNumber = [
  { name: "name1", count: 13 },
  { name: "name3", count: 23 },
  { name: "name1", count: 25 },
  { name: "name2", count: 27 },
  { name: "name3", count: 30 },
  { name: "name2", count: 20 },
];

const mergerArray = [];

arrNumber.forEach((arr) => {
  const checkArrExistIndex = mergerArray.findIndex((x) => x.name === arr.name);
  if (checkArrExistIndex !== -1) {
    mergerArray[checkArrExistIndex].count += arr.count;
  } else {
    mergerArray.push(arr);
  }
});
console.log(mergerArray);
