// Viết code chuyển mảng đã sắp xếp ở bài 1 thành mảng tên
// VD: [‘name4’, ‘name2’, ‘name3’]

const ageList = [
  { name: "name1", age: 12 },
  { name: "name2", age: 20 },
  { name: "name3", age: 15 },
  { name: "name4", age: 10 },
  { name: "name4", age: 27 },
];
const newArray = ageList.sort((a, b) => b.age - a.age);

const arrayName = newArray.map((x) => {
  return x.name;
});
const data = arrayName.filter((name, index) => {
  return arrayName.findIndex((x) => x == name) == index;
});

console.log(data);
